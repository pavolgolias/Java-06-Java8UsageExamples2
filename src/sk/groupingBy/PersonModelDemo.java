package sk.groupingBy;

public class PersonModelDemo {
    private String name;
    private int age;

    PersonModelDemo(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "PersonModelDemo{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
