package sk.groupingBy;

// examples downloaded from:
//  - https://dzone.com/articles/java-8-group-collections
//  - http://www.oracle.com/technetwork/articles/java/architect-streams-pt2-2227132.html
//  - https://thecannycoder.wordpress.com/2014/08/15/collectors-part-2-provided-collectors-and-a-java-8-streams-demonstration/

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

public class GroupingByExamples {
    public static void groupingByExample1() {
        Stream<PersonModelDemo> people = Stream.of(new PersonModelDemo("Man 1", 24), new PersonModelDemo("Man 2", 30),
                new PersonModelDemo("Man 3", 24), new PersonModelDemo("Man 4", 28));

        Map<Integer, List<String>> peopleByAge = people
                .collect(
                        groupingBy(PersonModelDemo::getAge, Collectors.mapping(PersonModelDemo::getName, toList())));

        System.out.println(peopleByAge);
    }

    public static void groupingByExample2() {
        Stream<String> words = Stream.of("Java", "is", "the", "best");
        Map<String, Long> letterToCount = words
                .map(w -> w.split(""))
                .flatMap(Arrays::stream)
                .collect(groupingBy(identity(), counting()));

        System.out.println(letterToCount);
    }

    public static void groupingByExample3() {
        List<String> list = Arrays.asList("Hello", "Hello", "Hello", "Hi", "Hi", "hi");
        Map<String, Long> counts = list.stream().collect(groupingBy(identity(), counting()));

        System.out.println(counts);
    }

    public static void groupingByExample4() {
        Integer[] numbersArray = new Integer[]{1, 2, 3, 4, 5, 6};

        System.out.println("\nNumbers Array examples:");

        System.out.println(Arrays.stream(numbersArray).collect(Collectors.counting()));

        System.out.println(Arrays.stream(numbersArray).collect(Collectors.summingInt((Integer x) -> x)));

        System.out.println(Arrays.stream(numbersArray).collect(Collectors.averagingInt((Integer x) -> x)));

        System.out.println(Arrays.stream(numbersArray).collect(Collectors.maxBy(Integer::compare)).get());

        System.out.println(Arrays.stream(numbersArray).collect(Collectors.minBy(Integer::compare)).get());

        System.out.println(Arrays.stream(numbersArray).collect(Collectors.summarizingInt((Integer x) -> x)));
    }

    public static void groupingByExample5() {
        Character[] chars = new Character[]{'a', 'b', 'c', 'd', 'e', 'f', 'g'};

        System.out.println("\nCharacter Array examples:");

        // First a list
        List<Character> l = Arrays.stream(chars).collect(Collectors.toList());
        System.out.println(l);

        // toList gives us a generic list (code creates an ArrayList), let's get a linked list
        List<Character> ll = Arrays.stream(chars).collect(Collectors.toCollection(LinkedList::new));
        System.out.println(ll);

        // toSet gives us a generic set (code creates a HashSet)
        Set<Character> s = Arrays.stream(chars).collect(Collectors.toSet());
        System.out.println(s);

        // and now a generic map (code creates a HashMap)
        Map<Character, Character> m = Arrays.stream(chars)
                .collect(Collectors.toMap(Character::toUpperCase, Function.identity()));
        System.out.println(m);

        // What happens if keys clash?
        try {
            Arrays.stream(chars).collect(Collectors.toMap((Character k) -> 'a', Function.identity()));
        } catch (IllegalStateException e) {
            System.out.println("Expected: Caught duplicate key");
        }

        // Let's provide a function to resolve this
        // we'll keep the first
        Map<Character, Character> m2 = Arrays.stream(chars)
                .collect(Collectors.toMap((Character k) -> 'a', Function.identity(), (v1, v2) -> v1));
        System.out.println(m2);

        // If we return null from our merge function,
        // the latest is kept
        Map<Character, Character> m3 = Arrays.stream(chars)
                .collect(Collectors.toMap((Character k) -> 'a', Function.identity(), (v1, v2) -> null));
        System.out.println(m3);

        // We can also request a different type of map
        Map<Character, Character> m4 = Arrays.stream(chars)
                .collect(Collectors
                        .toMap(Character::toUpperCase, Function.identity(), (v1, v2) -> v1, TreeMap::new));
        System.out.println(m4);
    }

    public static void groupingByExample6() {
        Character[] chars = new Character[]{'a', 'b', 'c', 'd', 'e', 'f', 'g'};

        System.out.println("\nCharacter Array with strings examples:");
        // Join them all together
        System.out.println(Arrays.stream(chars).map(Object::toString).collect(Collectors.joining()));

        // Join with a ,
        System.out.println(Arrays.stream(chars).map(Object::toString).collect(Collectors.joining(",")));

        // Join with a , and surround the whole thing with []
        System.out.println(Arrays.stream(chars)
                .map(Object::toString)
                .collect(Collectors.joining(",", "[", "]")));

        // Group into two groups
        Map<String, List<Character>> group1 = Arrays.stream(chars)
                .collect(Collectors.groupingBy((Character x) -> x < 'd' ? "Before_D" : "D_Onward"));
        System.out.println(group1);

        // As before, but group values with like keys in a set
        Map<String, Set<Character>> group2 = Arrays.stream(chars)
                .collect(Collectors.groupingBy((Character x) -> x < 'd' ? "Before_D" : "D_Onward", Collectors.toSet()));
        System.out.println(group2);

        // Put the whole grouping structure in a TreeMap
        Map<String, Set<Character>> group3 = Arrays.stream(chars)
                .collect(Collectors.groupingBy((Character x) -> x < 'd' ? "Before_D" : "D_Onward",
                        TreeMap::new,
                        Collectors.toSet()));
        System.out.println(group3);

        // Partition into two lists
        Map<Boolean, List<Character>> partition1 = Arrays.stream(chars)
                .collect(Collectors.partitioningBy((Character x) -> x < 'd'));
        System.out.println(partition1);

        // Partition into two sets
        Map<Boolean, Set<Character>> partition2 = Arrays.stream(chars)
                .collect(Collectors.partitioningBy((Character x) -> x < 'd', Collectors.toSet()));
        System.out.println(partition2);
    }
}
