package sk;

import sk.groupingBy.GroupingByExamples;
import sk.optional.OptionalExamples;
import sk.predicate.PredicateExamples;
import sk.reduce.ReduceExamples;

public class Main {
    public static void main(String[] args) {
        //runOptionalExamples();
        //runGroupingByExamples();
        //runPredicateExamples();
        runReduceExamples();
    }

    private static void runReduceExamples() {
        ReduceExamples.reduceExample1();
        ReduceExamples.reduceExample2();
        ReduceExamples.reduceExample3();
    }

    private static void runPredicateExamples() {
        PredicateExamples.predicateExample1();
        PredicateExamples.predicateExample2WithEmployees();
    }

    private static void runGroupingByExamples() {
        GroupingByExamples.groupingByExample1();
        GroupingByExamples.groupingByExample2();
        GroupingByExamples.groupingByExample3();
        GroupingByExamples.groupingByExample4();
        GroupingByExamples.groupingByExample5();
        GroupingByExamples.groupingByExample6();
    }

    private static void runOptionalExamples() {
        System.out.println("Optional Examples");
        OptionalExamples.filterExample();
        OptionalExamples.mapExample();
        OptionalExamples.ifPresentExample();
        OptionalExamples.isPresentExample();
        OptionalExamples.orElseThrowExample();
        OptionalExamples.orElseExample();
        OptionalExamples.getExample();
        OptionalExamples.nullableOptional();
        OptionalExamples.nonEmptyOptional();
        OptionalExamples.emptyOptionalCreation();
    }
}
