package sk.reduce;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

// examples downloaded from: http://www.programcreek.com/2014/01/reduce-stream-examples/

public class ReduceExamples {
    private static List<String> list = new ArrayList<>();

    static {
        list.add("java");
        list.add("php");
        list.add("python");
        list.add("perl");
        list.add("c");
        list.add("c#");
    }

    public static void reduceExample1() {
        // how to get sum of words length?
        int s = list.stream().map(String::length)
                .mapToInt(Integer::new)
                .sum();

        System.out.println(s);

        //or...
        Stream<Integer> lengthStream = list.stream().map(String::length);
        Optional<Integer> sum = lengthStream.reduce((x, y) -> x + y);
        sum.ifPresent(System.out::println);
    }

    public static void reduceExample2() {
        Stream<Integer> lengthStream = list.stream().map(String::length);
        int sum = lengthStream.reduce(10, (x, y) -> x + y);
        System.out.println(sum);
    }

    public static void reduceExample3() {
        /* The three parameters are identify, reducer, and combiner.
            - identity - identity value for the combiner function
            - reducer - function for combining two results
            - combiner - function for adding an additional element into a result. */
        int s = list.stream().reduce(0, (x, y) -> x + y.length(), (x, y) -> x + y);
        System.out.println(s);
    }
}
