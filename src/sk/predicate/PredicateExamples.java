package sk.predicate;

// doc: https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html
// examples downloaded from:
//  - http://www.java2s.com/Tutorials/Java/java.util.function/Predicate/index.htm
//  - http://howtodoinjava.com/java-8/how-to-use-predicate-in-java-8/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static sk.predicate.EmployeePredicates.*;

public class PredicateExamples {
    public static void predicateExample1() {
        Predicate<String> i = (s) -> s.length() > 10;

        System.out.println(i.test("Java 8 is reaaaly awesome!"));
    }

    public static void predicateExample2WithEmployees() {
        EmployeeDemoModel e1 = new EmployeeDemoModel(1, 23, "M", "Rick", "Beethovan");
        EmployeeDemoModel e2 = new EmployeeDemoModel(2, 13, "F", "Martina", "Hengis");
        EmployeeDemoModel e3 = new EmployeeDemoModel(3, 43, "M", "Ricky", "Martin");
        EmployeeDemoModel e4 = new EmployeeDemoModel(4, 26, "M", "Jon", "Lowman");
        EmployeeDemoModel e5 = new EmployeeDemoModel(5, 19, "F", "Cristine", "Maria");
        EmployeeDemoModel e6 = new EmployeeDemoModel(6, 15, "M", "David", "Feezor");
        EmployeeDemoModel e7 = new EmployeeDemoModel(7, 68, "F", "Melissa", "Roy");
        EmployeeDemoModel e8 = new EmployeeDemoModel(8, 79, "M", "Alex", "Gussin");
        EmployeeDemoModel e9 = new EmployeeDemoModel(9, 15, "F", "Neetu", "Singh");
        EmployeeDemoModel e10 = new EmployeeDemoModel(10, 45, "M", "Naveen", "Jain");

        List<EmployeeDemoModel> employees = new ArrayList<>();
        employees.addAll(Arrays.asList(e1, e2, e3, e4, e5, e6, e7, e8, e9, e10));

        System.out.println(filterEmployees(employees, isAdultMale()));

        System.out.println(filterEmployees(employees, isAdultFemale()));

        System.out.println(filterEmployees(employees, isAgeMoreThan(35)));

        // Employees other than above collection of "isAgeMoreThan(35)" can be get using negate()
        // Can use negate(), and() or or()
        System.out.println(filterEmployees(employees, isAgeMoreThan(35).negate()));
    }
}
