package sk.predicate;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class EmployeePredicates {
    public static Predicate<EmployeeDemoModel> isAdultMale() {
        return p -> p.getAge() > 21 && p.getGender().equalsIgnoreCase("M");
    }

    public static Predicate<EmployeeDemoModel> isAdultFemale() {
        return p -> p.getAge() > 18 && p.getGender().equalsIgnoreCase("F");
    }

    public static Predicate<EmployeeDemoModel> isAgeMoreThan(Integer age) {
        return p -> p.getAge() > age;
    }

    public static List<EmployeeDemoModel> filterEmployees(List<EmployeeDemoModel> employees,
            Predicate<EmployeeDemoModel> predicate) {

        return employees.stream().filter(predicate).collect(Collectors.toList());
    }
}
