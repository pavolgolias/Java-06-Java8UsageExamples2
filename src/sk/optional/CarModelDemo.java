package sk.optional;

public class CarModelDemo {
    private String price;

    public CarModelDemo(String price) {
        setPrice(price);
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "CarModelDemo{" +
                "price='" + price + '\'' +
                '}';
    }
}
