package sk.optional;

import java.util.NoSuchElementException;
import java.util.Optional;

//documentation: https://docs.oracle.com/javase/8/docs/api/java/util/Optional.html
//examples downloaded from: https://examples.javacodegeeks.com/core-java/util/optional/java-8-optional-example/

public class OptionalExamples {
    public static void filterExample() {
        // if the value is not present
        Optional<CarModelDemo> carModelDemoOptionalEmpty = Optional.empty();
        carModelDemoOptionalEmpty
                .filter(x -> "250".equals(x.getPrice()))
                .ifPresent(x -> System.out.println(x.getPrice() + " is ok!"));

        // if the value does not pass the filter
        Optional<CarModelDemo> carModelDemoOptionalExpensive = Optional.of(new CarModelDemo("3333"));
        carModelDemoOptionalExpensive
                .filter(x -> "250".equals(x.getPrice()))
                .ifPresent(x -> System.out.println(x.getPrice() + " is ok!"));

        // if the value is present and does pass the filter
        Optional<CarModelDemo> carModelDemoOptionalOk = Optional.of(new CarModelDemo("250"));
        carModelDemoOptionalOk
                .filter(x -> "250".equals(x.getPrice()))
                .ifPresent(x -> System.out.println(x.getPrice() + " is ok!"));
    }

    public static void mapExample() {
        // non empty string map to its length
        Optional<String> stringOptional = Optional.of("loooooooong string");
        // map from Optional<String> to Optional<Integer>
        Optional<Integer> sizeOptional = stringOptional.map(String::length);

        System.out.println("size of string " + sizeOptional.orElse(0));

        // empty string map to its length -> we get 0 as length
        Optional<String> stringOptionalNull = Optional.ofNullable(null);
        Optional<Integer> sizeOptionalNull = stringOptionalNull.map(String::length);

        System.out.println("size of string " + sizeOptionalNull.orElse(0));
    }

    public static void ifPresentExample() {
        Optional<String> stringToUse = Optional.of("test string");
        stringToUse.ifPresent(System.out::println);

        Optional<String> stringToUseNull = Optional.ofNullable(null);
        stringToUseNull.ifPresent(System.out::println);
    }

    public static void isPresentExample() {
        Optional<String> stringToUse = Optional.of("isPresentExample");
        //Optional<String> stringToUse = Optional.ofNullable(null);
        if (stringToUse.isPresent()) {
            System.out.println(stringToUse.get());
        }
    }

    public static void orElseThrowExample() {
        try {
            CarModelDemo CarModelDemoNull = null;
            Optional<CarModelDemo> optionalCarModelDemoNull = Optional.ofNullable(CarModelDemoNull);
            // orElseThrow == Return the contained value, if present, otherwise throw an exception to be created by the provided supplier.
            optionalCarModelDemoNull.orElseThrow(IllegalStateException::new);
        } catch (IllegalStateException ex) {
            System.out.println("expected IllegalStateException");
        }
    }

    public static void orElseExample() {
        CarModelDemo carModelDemoCreated = new CarModelDemo("500");
        CarModelDemo defaultCarModelDemo = new CarModelDemo("250");

        // value is present
        Optional<CarModelDemo> optionalCarModelDemo = Optional.of(carModelDemoCreated);
        String price = optionalCarModelDemo.orElse(defaultCarModelDemo).getPrice();
        System.out.println("CarModelDemo price: " + price);

        // else not
        Optional<CarModelDemo> optionalCarModelDemo2 = Optional.empty();
        price = optionalCarModelDemo2.orElse(defaultCarModelDemo).getPrice();
        System.out.println("CarModelDemo price: " + price);
    }

    public static void getExample() {
        try {
            String strNull = null;
            // null cannot be passed to of(), we should use ofNullable() !
            Optional<String> optionalString = Optional.of(strNull);
            // we will not get here, the previous line throws NullPointerException
            if (optionalString.isPresent()) {
                System.out.println(optionalString.get().contains("something"));
            }
        } catch (NullPointerException ex) {
            System.out.println("expected NullPointerException");
        }
    }

    public static void nullableOptional() {
        try {
            String strNull = null;
            Optional<String> nullableOptional = Optional.ofNullable(strNull);
            System.out.println(nullableOptional.get());
        } catch (NoSuchElementException ex) {
            System.out.println("expected NoSuchElementException");
        }
    }

    public static void nonEmptyOptional() {
        String str = "test non empty string";
        Optional<String> nonEmptyOptional = Optional.of(str);
        if (nonEmptyOptional.isPresent()) {
            System.out.println(nonEmptyOptional.get());
        } else {
            System.out.println("No value is present");
        }
    }

    public static void emptyOptionalCreation() {
        try {
            Optional<String> emptyOptional = Optional.empty();
            System.out.println(emptyOptional.get());
        } catch (NoSuchElementException ex) {
            System.out.println("expected NoSuchElementException");
        }
    }
}
